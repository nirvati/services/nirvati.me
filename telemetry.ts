Deno.serve(async (req: Request) => {
    if (req.method == "POST") {
        // Ensure version and id are present and are strings
        const body = await req.json();
        if (typeof body.nirvatiVersion !== "string" || typeof body.id !== "string") {
            return new Response("Bad request", { status: 400 });
        }
        const resp = await fetch("https://wfhqswpvrsuvsmevtnrp.supabase.co/functions/v1/add_telemetry_event", {
            method: "POST",
            headers: {
                "Authorization": Deno.env.get("SUPABASE_SERVICE_KEY") ?? "",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        });
        return new Response(await resp.text(), { status: resp.status });
    } else {
        return new Response("Method not allowed", { status: 405 });
    }
});
