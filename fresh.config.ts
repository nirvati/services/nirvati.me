import { defineConfig } from "$fresh/server.ts";
import tailwind from "$fresh/plugins/tailwind.ts";
import { TurnstilePlugin } from "./plugins/turnstile/index.ts";

export default defineConfig({
  plugins: [tailwind(), TurnstilePlugin()],
});
