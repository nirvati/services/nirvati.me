import { useEffect, useState } from "preact/hooks";
import { decodeBase64 } from "https://deno.land/std@0.208.0/encoding/base64.ts";

export default function ValidatedInput(
  props: { name: string; placeholder: string },
) {
  const [value, setValue] = useState<string | null>(null);
  const [error, setError] = useState(false);

  useEffect(() => {
    try {
      if (value === null) return;
      const textDecoder = new TextDecoder();
      const decoded = textDecoder.decode(decodeBase64(value));
      const { username, password, ip } = JSON.parse(decoded);
      if (
        typeof username !== "string" || typeof password !== "string" ||
        typeof ip !== "string"
      ) {
        throw new Error();
      }
      setError(false);
    } catch {
      setError(true);
    }
  }, [value]);

  return (
    <input
      value={value as string}
      name={props.name}
      type="text"
      placeholder={props.placeholder}
      class={`border-0 bg-transparent border-b-2 p-2 border-black dark:border-white text-lg text-center lg:w-[45rem] ${
        error ? "border-red-400 dark:border-red-900" : ""
      }`}
      onInput={(ev) =>
        setValue((ev.target! as unknown as HTMLInputElement).value)}
    />
  );
}
