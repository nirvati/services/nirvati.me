import { CfTurnstileValidationResult } from "../plugins/turnstile/handlers/CfTurnstileValidation.ts";
import { Handlers } from "$fresh/server.ts";
import supabase from "../utils/supabase.ts";
import { hashSync } from "https://deno.land/x/bcrypt@v0.4.1/mod.ts";
import { Head } from "$fresh/runtime.ts";
import { decodeBase64 } from "https://deno.land/std@0.208.0/encoding/base64.ts";

export const handler: Handlers = {
  async POST(req, ctx) {
    const secretKey = Deno.env.get("TURNSTILE_SERVER_KEY")!;

    if (!secretKey) {
      throw new Error("Missing TURNSTILE_SERVER_KEY");
    }
    const body = await req.formData();
    // Turnstile injects a token in "cf-turnstile-response".
    const token = body.get("cf-turnstile-response");
    const ip = (ctx.remoteAddr as Deno.NetAddr).hostname;
    if (!token) {
      return new Response("Missing captcha", {
        status: 400,
      });
    }
    
    /*if (body.get("no-extremists") !== "on") {
      return new Response("Nirvati.me is not available to members of the Alternative für Deutschland party or any organization considered extremist.", {
        status: 403,
      });
    }*/

    const formData = new FormData();
    formData.append("secret", secretKey);
    formData.append("response", token);
    formData.append("remoteip", ip);

    const url = "https://challenges.cloudflare.com/turnstile/v0/siteverify";
    const result = await fetch(url, {
      body: formData,
      method: "POST",
    });

    const outcome: CfTurnstileValidationResult = await result.json();

    if (outcome.success) {
      const loginKey = body.get("login-code");
      let subdomain = body.get("subdomain");
      if (
        typeof loginKey !== "string" ||
        typeof subdomain !== "string"
      ) {
        return new Response("Bad request", {
          status: 400,
        });
      }
      let username: string, password: string, ip: string;
      try {
        const textDecoder = new TextDecoder();
        const decoded = textDecoder.decode(decodeBase64(loginKey));
        const data = JSON.parse(decoded);
        username = data.username;
        password = data.password;
        ip = data.ip;
        if (
          typeof username !== "string" || typeof password !== "string" ||
          typeof ip !== "string" ||
          !ip.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) ||
          !username.match(/^[a-zA-Z0-9]{64}$/) ||
          !password.match(/^[a-zA-Z0-9]{64}$/)
        ) {
          throw new Error();
        }
        // Also ensure the IP is private or in the 100.64.0.0/10 range.
        const ipParts = ip.split(".").map((part) => parseInt(part));
        if (
          !((ipParts[0] == 10) ||
            (ipParts[0] == 100 && ipParts[1] >= 64 && ipParts[1] <= 127) ||
            (ipParts[0] == 172 && ipParts[1] >= 16 && ipParts[1] <= 31) ||
            (ipParts[0] == 192 && ipParts[1] == 168))
        ) {
          throw new Error();
        }
      } catch {
        return new Response(
          "Please make sure you copy your login data exactly from Nirvati.",
          {
            status: 400,
          },
        );
      }
      subdomain = subdomain.toLowerCase();
      if (
        subdomain.length < 5 ||
        !subdomain.match(/^[a-z0-9]+$/) || subdomain.length >= 20
      ) {
        return new Response("Invalid subdomain", {
          status: 400,
        });
      }

      const { error } = await supabase.from("domains").insert({
        owner: username,
        password: hashSync(password),
        ip,
        name: subdomain,
      });
      if (error) {
        console.error(error);
        return new Response("Failed to register subdomain", {
          status: 500,
        });
      }
    } else {
      if (!token || !secretKey) {
        return new Response(
          "Failed to validate captcha. Are you a robot?",
          {
            status: 400,
          },
        );
      }
    }
    return ctx.render();
  },
};

export default function SuccessPage() {
  return (
    <>
      <Head>
        <title>Subdomain set up</title>
      </Head>
      <div class="dark:bg-gray-800 dark:text-white min-h-screen">
        <div class="min-h-screen flex items-center justify-center flex-col">
          <h1 class="text-7xl font-semibold tracking-tight leading-none md:text-8xl xl:text-9xl mb-4">
            Your subdomain is set up!
          </h1>
          <p>
            You can now continue the setup on your server.
          </p>
        </div>
      </div>
    </>
  );
}
