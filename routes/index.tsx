import { Head } from "$fresh/runtime.ts";

import CfTurnstile from "../plugins/turnstile/components/CfTurnstile.tsx";
import SubDomainInput from "../islands/SubDomainInput.tsx";
import ValidatedInput from "../islands/ValidatedInput.tsx";

export default function Home() {
  return (
    <>
      <Head>
        <title>Sign up for your subdomain</title>
      </Head>
      <div class="dark:bg-gray-800 dark:text-white min-h-screen">
        <div class="min-h-screen flex items-center justify-center flex-col">
          <form
            method="POST"
            action="/submit"
            class="flex items-center justify-center flex-col"
          >
            <SubDomainInput />
            <div class="text-xl mt-2 mb-1">
              Your login code:{" "}
              <ValidatedInput
                name="login-code"
                placeholder="eyJ1c2VybmFtZSI6ICJBIHNlY3JldCIsICJwYXNzd29yZCI6ICJTb21ldGhpbmciLCAiaXAiOiAiMTAwLjAuMC4wIn0="
              />
            </div>
            <div class="max-w-xl flex flex-row items-center justify-center my-3">
            {/*<input name="no-extremists" id="no-extremists" type="checkbox"></input>
            <label for="no-extremists" class="inline ml-3 text-justify">By using this service, I guarantee that I'm not a member of the "Alternative für Deutschland" party, or any party or organization considered extremist by the German Federal Office for the Protection of the Constitution.</label>*/}
            </div><CfTurnstile sitekey={Deno.env.get("TURNSTILE_SITE_KEY")!} />
            <button
              type="submit"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-4"
            >
              Finish
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
