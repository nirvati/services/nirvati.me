import { Handlers } from "$fresh/server.ts";
import { validateAuthFromRequest } from "../../../utils/auth.ts";
import cf from "../../../utils/cloudflare.ts";
import { DnsCaaRecord, DnsTxtRecord } from "../../../utils/cloudflare/index.ts";

const zoneId = Deno.env.get("CLOUDFLARE_ZONE_ID")!;

type DnsRecordInput = DnsCaaRecord | DnsTxtRecord;

export const handler: Handlers = {
  async POST(req, _ctx) {
    const authData = await validateAuthFromRequest(req);
    if (authData instanceof Response) {
      console.error(`[401] /api/dns/create: Missing auth data.`);
      return authData;
    }
    const body = await req.json();
    if (
      typeof body !== "object" || !body.name || !body.type ||
      (!body.content && !body.data)
    ) {
      return new Response("Missing required properties", { status: 400 });
    }
    if (body.type !== "TXT" && body.type !== "CAA") {
      return new Response("Only TXT and CAA records are supported currently.", {
        status: 400,
      });
    }
    // Ensure user has access to body.name
    if (
      authData.name !== body.name && !body.name.endsWith(`.${authData.name}`)
    ) {
      return new Response(
        `Your domain needs to be a subdomain of ${authData.name}.nirvati.me. ${body.name}.nirvati.me is not a subdomain of ${authData.name}.nirvati.me.`,
        { status: 401 },
      );
    }
    const res = await cf.createDnsRecord(zoneId, {
      name: body.name,
      type: body.type,
      ...(body.content ? { content: body.content } : {}),
      ...(body.data ? { data: body.data } : {}),
      ttl: body.ttl || 60,
      proxied: false,
    } as DnsRecordInput);
    return new Response(JSON.stringify(res), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  },
};
