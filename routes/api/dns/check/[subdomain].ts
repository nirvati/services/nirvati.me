import { FreshContext, Handlers } from "$fresh/server.ts";
import supabase from "../../../../utils/supabase.ts";

export const handler: Handlers = {
  async GET(_req: Request, ctx: FreshContext) {
    const subdomain = ctx.params.subdomain as string;
    const { count, error } = await supabase.from("domains").select("name", {
      count: "exact",
      head: true,
    }).eq("name", subdomain);
    if (error) {
      console.error(error);
      return new Response(error.message, { status: 500 });
    }
    return new Response(JSON.stringify({ available: count === 0 }), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  },
};
