import { FreshContext } from "$fresh/server.ts";
import { validateAuthFromRequest } from "../../../utils/auth.ts";

export const handler = async (
  req: Request,
  _ctx: FreshContext,
): Promise<Response> => {
  const validated = await validateAuthFromRequest(req);
  if (validated instanceof Response) {
    return validated;
  } else {
    return new Response(JSON.stringify(validated), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  }
};
