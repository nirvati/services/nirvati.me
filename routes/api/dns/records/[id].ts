import cf from "../../../../utils/cloudflare.ts";
import { Handlers } from "$fresh/server.ts";
import { validateAuthFromRequest } from "../../../../utils/auth.ts";
import {
  DnsCaaRecord,
  DnsTxtRecord,
} from "../../../../utils/cloudflare/index.ts";

const zoneId = Deno.env.get("CLOUDFLARE_ZONE_ID")!;

type DnsRecordInput = DnsCaaRecord | DnsTxtRecord;

export const handler: Handlers = {
  async GET(req, ctx) {
    const id = ctx.params.id;
    const authData = await validateAuthFromRequest(req);
    if (authData instanceof Response) {
      return authData;
    }
    const res = await cf.getDnsRecord(zoneId, id);
    if (
      !res || !res.name ||
      `${authData.name}.nirvati.me` !== res.name && !res.name.endsWith(`.${authData.name}.nirvati.me`)
    ) {
      return new Response("Unauthorized", { status: 401 });
    }
    return new Response(JSON.stringify(res), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  },
  async PUT(req, ctx) {
    const id = ctx.params.id;
    const authData = await validateAuthFromRequest(req);
    if (authData instanceof Response) {
      return authData;
    }
    const input = await req.json() as DnsRecordInput;
    if (
      !input.type || (!input.content && !(input as DnsCaaRecord).data) ||
      !input.ttl
    ) {
      return new Response("Missing required fields", { status: 400 });
    }
    if (input.type !== "TXT" && input.type !== "CAA") {
      return new Response("Only TXT and CAA records are supported currently.", {
        status: 400,
      });
    }
    // Get the ID first and ensure user has perms
    const res = await cf.getDnsRecord(zoneId, id);
    if (
      !res || !res.name ||
      `${authData.name}.nirvati.me` !== res.name && !res.name.endsWith(`.${authData.name}.nirvati.me`)
    ) {
      return new Response("Unauthorized", { status: 401 });
    }
    const updateRes = await cf.updateDnsRecord(zoneId, id, {
      name: res.name,
      type: input.type,
      ...(input.content ? { content: input.content } : {}),
      ttl: input.ttl || 60,
      ...(input.type === "CAA" && (input as DnsCaaRecord).data ? { data: (input as DnsCaaRecord).data } : {}),
    } as DnsRecordInput);
    return new Response(JSON.stringify(updateRes), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  },
  async DELETE(req, ctx) {
    const id = ctx.params.id;
    const authData = await validateAuthFromRequest(req);
    if (authData instanceof Response) {
      return authData;
    }
    const res = await cf.getDnsRecord(zoneId, id);
    if (
      !res || !res.name ||
      `${authData.name}.nirvati.me` !== res.name && !res.name.endsWith(`.${authData.name}.nirvati.me`)
    ) {
      return new Response("Unauthorized", { status: 401 });
    }
    await cf.deleteDnsRecord(zoneId, id);
    return new Response(null, { status: 204 });
  },
};
