import cf from "../../../utils/cloudflare.ts";
import { Handlers } from "$fresh/server.ts";
import { DnsRecordType } from "https://cdn.skypack.dev/cloudflare-client?dts";
import { validateAuthFromRequest } from "../../../utils/auth.ts";

const zoneId = Deno.env.get("CLOUDFLARE_ZONE_ID")!;

type FindOptions = {
  /**
   * DNS record name (`max length: 255`)
   * @example "example.com"
   */
  name: string;
  /**
   * DNS record type
   * @example "A"
   */
  type?: DnsRecordType;
  content?: string;
};

export const handler: Handlers = {
  async GET(req, _ctx) {
    const authData = await validateAuthFromRequest(req);
    if (authData instanceof Response) {
      return authData;
    }
    const url = new URL(req.url);
    const name = url.searchParams.get("name");
    const type = url.searchParams.get("type");
    const content = url.searchParams.get("content");
    if (!name) {
      return new Response("Missing name query parameter", { status: 400 });
    }
    // Ensure user has access to body.name
    if (authData.name !== name && !name.endsWith(`.${authData.name}`)) {
      return new Response("Unauthorized", { status: 401 });
    }
    const res = await cf.findDnsRecord(zoneId, name + ".nirvati.me", type as DnsRecordType, content);
    return new Response(JSON.stringify(res), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  },
};
