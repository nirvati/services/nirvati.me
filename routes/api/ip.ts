import { FreshContext, Handlers } from "$fresh/server.ts";
import { validateAuthFromRequest } from "../../utils/auth.ts";
import supabase from "../../utils/supabase.ts";
import { ensureNoInvalidProps } from "../../utils/validateBody.ts";

export const handler: Handlers = {
  async PUT(req: Request, _ctx: FreshContext) {
    const validated = await validateAuthFromRequest(req);
    if (validated instanceof Response) {
      return validated;
    } else {
      const rawBody = await req.json();
      const [body, error] = ensureNoInvalidProps<{ ip: string }>(
        rawBody,
        ["ip"],
        ["ip"],
      );
      if (error) {
        return error;
      }
      // The IP is further validated in the database itself
      supabase
        .from("domains")
        .update({ ip: body.ip })
        .eq("name", validated.name);
      return new Response(JSON.stringify(validated), {
        status: 200,
        headers: { "Content-Type": "application/json" },
      });
    }
  },
};
