import { Tables } from "./db.ts";
import * as Cloudflare from "npm:cloudflare-client";

type InsertPayload = {
  type: "INSERT";
  table: string;
  schema: string;
  record: Tables<"domains">;
  old_record: null;
};
type UpdatePayload = {
  type: "UPDATE";
  table: string;
  schema: string;
  record: Tables<"domains">;
  old_record: Tables<"domains">;
};
type DeletePayloady = {
  type: "DELETE";
  table: string;
  schema: string;
  record: null;
  old_record: Tables<"domains">;
};

type Payload = InsertPayload | UpdatePayload | DeletePayloady;

Deno.serve(async (req: Request) => {
  const authHeader = req.headers.get("Authorization");
  // Auth header should be validated before, but ensure it does not match a regular user or anon key
  const authParts = authHeader?.split(" ");
  const parsedJwt = JSON.parse(atob(authParts![1].split(".")[1]));
  if (
    authParts![0] !== "Bearer" ||
    parsedJwt.role !== "service_role"
  ) {
    return new Response("Unauthorized", { status: 401 });
  }
  const body = (await req.json()) as Payload;
  console.debug(body);
  const dnsRecords = Cloudflare.dnsRecords({
    accessToken: Deno.env.get("CF_APITOKEN") ?? "",
    zoneId: Deno.env.get("CF_ZONEID") ?? "",
  });
  switch (body.type) {
    case "INSERT": {
      dnsRecords.create({
        type: "A",
        name: `*.${body.record.name}`,
        content: body.record.ip,
        ttl: 300,
        proxied: false,
      });
      dnsRecords.create({
        type: "A",
        name: `${body.record.name}`,
        content: body.record.ip,
        ttl: 300,
        proxied: false,
      });
      break;
    }
    case "UPDATE": {
      const oldRecordWildcard = await dnsRecords
        .find({ name: `*.${body.old_record.name}` })
        .first();
      dnsRecords.update(oldRecordWildcard!.id, {
        type: "A",
        name: `*.${body.record.name}`,
        content: body.record.ip,
        ttl: 300,
        proxied: false,
      });
      const oldRecord = await dnsRecords
        .find({ name: `${body.old_record.name}` })
        .first();
      dnsRecords.update(oldRecord!.id, {
        type: "A",
        name: `${body.record.name}`,
        content: body.record.ip,
        ttl: 300,
        proxied: false,
      });
      break;
    }
    case "DELETE": {
      const oldRecordWildcard = await dnsRecords
        .find({ name: `*.${body.old_record.name}` })
        .first();
      dnsRecords.delete(oldRecordWildcard!.id);
      const oldRecord = await dnsRecords
        .find({ name: `${body.old_record.name}` })
        .first();
      dnsRecords.delete(oldRecord!.id);
      break;
    }
  }
  return new Response(JSON.stringify({ success: true }), { status: 200 });
});
