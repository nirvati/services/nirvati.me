import { Database } from "./db.ts";
import { createClient } from 'https://esm.sh/@supabase/supabase-js@2'

type Payload = {
  nirvatiVersion: string;
  id: string;
}
Deno.serve(async (req: Request) => {
  const authHeader = req.headers.get("Authorization");
  // Auth header should be validated before, but ensure it does not match a regular user or anon key
  const authParts = authHeader?.split(" ");
  const parsedJwt = JSON.parse(atob(authParts![1].split(".")[1]));
  if (
    authParts![0] !== "Bearer" ||
    parsedJwt.role !== "service_role"
  ) {
    return new Response("Unauthorized", { status: 401 });
  }
  const body = (await req.json()) as Payload;
  const supabaseClient = createClient<Database>(
    Deno.env.get('SUPABASE_URL') ?? '',
    Deno.env.get('SUPABASE_ANON_KEY') ?? '',
    { global: { headers: { Authorization: authHeader! } } }
  );
  
  const { error } = await supabaseClient.from('telemetry_events').insert([
    {
      installation_id: body.id,
      version: body.nirvatiVersion,
    }
  ]);
  
  if (error) {
    console.error(error);
    return new Response("Internal server error", { status: 500 });
  }
  
  return new Response(JSON.stringify({ success: true }), { status: 200 });
});
