import { CloudflareClient } from "./cloudflare/index.ts";

const cf = new CloudflareClient(Deno.env.get("CLOUDFLARE_API_TOKEN")!);
export default cf;

export type { DnsRecord } from "./cloudflare/index.ts";
