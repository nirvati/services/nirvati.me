import createClient from "npm:openapi-fetch";
import type { components, paths } from "./cloudflare.d.ts"; // generated from openapi-typescript

export type DnsRecord = components["schemas"]["FwWbKTGv_dns-record"];
export type DnsTxtRecord = components["schemas"]["FwWbKTGv_TXTRecord"];
export type DnsCaaRecord = components["schemas"]["FwWbKTGv_CAARecord"];

export class CloudflareClient {
  client: ReturnType<typeof createClient<paths>>;
  constructor(apiToken: string) {
    this.client = createClient<paths>({
      baseUrl: "https://api.cloudflare.com/client/v4",
      headers: {
        Authorization: `Bearer ${apiToken}`,
      },
    });
  }

  async createDnsRecord(zone: string, record: DnsRecord) {
    const res = await this.client.POST("/zones/{zone_identifier}/dns_records", {
      params: {
        path: {
          zone_identifier: zone,
        },
      },
      body: record,
    });
    if ((res.data?.errors.length || 0) > 0) {
      throw new Error(JSON.stringify(res.data?.errors));
    }
    return res.data?.result;
  }

  async deleteDnsRecord(zone: string, id: string) {
    const res = await this.client.DELETE(
      "/zones/{zone_identifier}/dns_records/{identifier}",
      {
        params: {
          path: {
            zone_identifier: zone,
            identifier: id,
          },
        },
      },
    );
    // @ts-expect-error Currently typings are broken
    if ((res.data?.errors.length || 0) > 0) {
      // @ts-expect-error Currently typings are broken
      throw new Error(JSON.stringify(res.data?.errors));
    }
  }

  async findDnsRecord(zone: string, name: string, type?: DnsRecord["type"] | null, content?: string | null) {
    const res = await this.client.GET("/zones/{zone_identifier}/dns_records", {
      params: {
        path: {
          zone_identifier: zone,
        },
        query: {
          name,
          ...(type ? { type } : {}),
          ...(content ? { content } : {}),
        },
      },
    });
    if ((res.data?.errors.length || 0) > 0) {
      throw new Error(JSON.stringify(res.data?.errors));
    }
    return res.data?.result! || [];
  }

  async updateDnsRecord(zone: string, id: string, record: DnsRecord) {
    const res = await this.client.PUT(
      "/zones/{zone_identifier}/dns_records/{identifier}",
      {
        params: {
          path: {
            zone_identifier: zone,
            identifier: id,
          },
        },
        body: record,
      },
    );
    if ((res.data?.errors.length || 0) > 0) {
      throw new Error(JSON.stringify(res.data?.errors));
    }
  }

  async patchDnsRecord(zone: string, id: string, record: DnsRecord) {
    const res = await this.client.PATCH(
      "/zones/{zone_identifier}/dns_records/{identifier}",
      {
        params: {
          path: {
            zone_identifier: zone,
            identifier: id,
          },
        },
        body: record,
      },
    );
    if ((res.data?.errors.length || 0) > 0) {
      throw new Error(JSON.stringify(res.data?.errors));
    }
  }

  async getDnsRecord(zone: string, id: string) {
    const res = await this.client.GET(
      "/zones/{zone_identifier}/dns_records/{identifier}",
      {
        params: {
          path: {
            zone_identifier: zone,
            identifier: id,
          },
        },
      },
    );
    if ((res.data?.errors.length || 0) > 0) {
      throw new Error(JSON.stringify(res.data?.errors));
    }
    return res.data?.result;
  }
}
